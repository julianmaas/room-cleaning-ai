import os, sys, pygame
from pygame.locals import *

from logic.helpers.constructgame import construct_game

from models.directions import Directions
from models.game import Game
from models.type import Type

# Game initialization variables
difficulty = 3
room_width, room_height = 20, 20
block_size = 40
# Pygame variables
window = width, height = block_size * room_width, block_size * room_height
color_grid = (217, 217, 217)
color_background = (255, 255, 255)
color_dirty = (255, 255, 255)
color_dock = (0, 112, 192)
color_clean = (146, 208, 80)
color_robot = (255, 192, 0)
color_furniture = (0, 0, 0)
framerate = 60.0
pygame.init()
clock = pygame.time.Clock()
screen = pygame.display.set_mode(window)

game = construct_game(room_width,room_height,difficulty)

# Handles input
def input(event):
    # Print statistics
    if (event == "p"): game.robot.print_statistics()
    # Print history
    if (event == "h"): game.robot.print_history()
    # Sense environment
    if (event == "space"): game.robot.sense(True)
    # Manual movement
    if(event == "up"): game.robot.move(Directions.UP)
    if(event == "down"): game.robot.move(Directions.DOWN)
    if(event == "left"): game.robot.move(Directions.LEFT)
    if(event == "right"): game.robot.move(Directions.RIGHT)
    # Astar to dock
    if(event == "r"): game.robot.dock()
    # Random
    if(event == "1"): game.robot.random()
    # Average random
    if(event == "2"): game.robot.average_random()
    # Zigzag
    if(event == "3"): game.robot.zigzag()
    # Search and clean dirty tiles
    if(event == "4"): game.robot.search_and_clean()
    # Complete zigzag that searches dirty tiles
    if(event == "5"): game.robot.complete_zigzag()
    # Explore map boundaries
    if(event == "6"): game.robot.explore_map_boundaries()
    # Clean and explore map
    if(event == "7"): game.robot.clean_and_explore_map()
    # Clean and explore map with zigzag
    if(event == "8"): game.robot.clean_and_explore_map_zigzag()
    # Make map dirty again
    if(event == "m"): game.room.make_dirty()
    # New map
    if(event == "return"): os.execl(sys.executable, os.path.abspath(__file__), *sys.argv)

# Draws room
def draw():
    pygame.event.pump()
    for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit()
    for x in range(0, width, block_size):
        for y in range(0, height, block_size):
            room = game.get_room_drawable()
            # Render non-cleaned tiles
            if room[y // block_size][x // block_size] == Type.DIRTY:
                rec = pygame.Rect(x, y, block_size, block_size)
                pygame.draw.rect(screen, color_dirty, rec)
            # Render cleaned tiles
            elif room[y // block_size][x // block_size] == Type.CLEAN:
                rec = pygame.Rect(x, y, block_size, block_size)
                pygame.draw.rect(screen, color_clean, rec)
            # Render docking station
            elif room[y // block_size][x // block_size] == Type.DOCK:
                rec = pygame.Rect(x, y, block_size, block_size)
                pygame.draw.rect(screen, color_dock, rec)
            # Render robot
            elif room[y // block_size][x // block_size] == Type.ROBOT:
                rec = pygame.Rect(x, y, block_size, block_size)
                pygame.draw.rect(screen, color_robot, rec)
            # Render furniture
            elif room[y // block_size][x // block_size] == Type.FURNITURE:
                rec = pygame.Rect(x, y, block_size, block_size)
                pygame.draw.rect(screen, color_furniture, rec)
            # Render grid
            rect = pygame.Rect(x, y, block_size, block_size)
            pygame.draw.rect(screen, color_grid, rect, 1)
    milliseconds = clock.tick(framerate)
    pygame.display.update()
    wait(milliseconds)

def wait(milliseconds):
    sleep_time = (1000.0 / framerate) - milliseconds
    if sleep_time > 0.0:
        pygame.time.wait(int(sleep_time))
    else:
        pygame.time.wait(1)

def main():
    game.draw = draw
    pygame.display.set_caption('Room cleaner')
    screen.fill(color_background)

    while 1:
        draw()
        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit()
            if event.type == KEYDOWN: key_name = input(pygame.key.name(event.key))

if __name__ == '__main__':
    main()