from models.type import Type
from logic.helpers.generateroom import generate_room

#  Construct a room from a set of sensed room parts
def construct_from_sensed(sensed_dictionary,robot_location_start,robot_location_end):
    unzipped = [[ y for y, x in sensed_dictionary ],
                [ x for y, x in sensed_dictionary ]]
    # Generate empty room
    room_width = max(unzipped[1]) - min(unzipped[1]) + 3
    room_height = max(unzipped[0]) - min(unzipped[0]) + 3
    room = generate_room(room_width, room_height, Type.UNKNOWN)

    for key, sensed_room in sensed_dictionary.items():
        for y, layer  in enumerate(sensed_room):
            for x, block in enumerate(layer):
                y_position = robot_location_start[0] + key[0] + y - 1
                x_position = robot_location_start[1] + key[1] + x - 1
                place_in_room(room, block, x_position, y_position)

    print("Initial sense:")
    for item in room:
        print(item)
    return room

# Places block in room at given position
def place_in_room(room, block, x, y):
    if x < 0 or y < 0 or x >= len(room[0]) or y >= len(room): return None # In bounds
    if block is Type.ROBOT: block = Type.CLEAN # Robot tile is cleaned
    if block is not Type.WALL and block is not Type.UNKNOWN: # Only in-bound blocks
        if room[y][x] is not Type.CLEAN: # Clean tiles don't get overwritten by dirty tiles
            room[y][x] = block
