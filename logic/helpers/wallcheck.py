from models.type import Type

# Check if a wall was spotted at the bounds of the sensed room
# A wall is detected as a wall if at least two tiles have been spotted
# Otherwise it can falsely detect an edge as a wall
# And then it can't correctly calculate width or height of the map

def left_wall_check(sensed):
    count = 0
    for layer in sensed:
        if layer[0] is Type.WALL:
            count += 1
    return count >= 2

def right_wall_check(sensed):
    count = 0
    for layer in sensed:
        if layer[-1] is Type.WALL:
            count += 1
    return count >= 2

def up_wall_check(sensed):
    count = 0
    for tile in sensed[0]:
        if tile is Type.WALL:
            count += 1
    return count >= 2

def down_wall_check(sensed):
    count = 0
    for tile in sensed[-1]:
        if tile is Type.WALL:
            count += 1
    return count >= 2