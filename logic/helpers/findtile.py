from logic.helpers.collisioncheck import collision_check

# Find coordinates of a specific tile
def find_tile(game,tile_type):
    room = game.get_room_drawable()

    # Initialize heuristic score with highest possible score
    lowest_heuristic_score = game.room.width * game.room.width * game.room.height * game.room.height
    most_optimal_cleaning_spot = None
    y = 0
    for layer in room:
        x = 0
        for tile in layer:
            if tile == tile_type:
                # If the tile is closer to robot than the last tile
                # Then target this tile as the most ideal tile to go to
                a = abs(game.robot.y-y)
                b = abs(game.robot.x-x)
                heuristic_score = int(round( (a*a) + (b*b) ))
                if (heuristic_score < lowest_heuristic_score):
                    lowest_heuristic_score = heuristic_score
                    most_optimal_cleaning_spot = (y,x)
            x += 1
        y += 1
    if most_optimal_cleaning_spot is not None:
        print("Added A* path to clean up (" + str(most_optimal_cleaning_spot[0]) + "," + str(most_optimal_cleaning_spot[1]) + ")")
    return most_optimal_cleaning_spot