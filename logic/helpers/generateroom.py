def generate_room(width,height,tile_type):
    room = []
    for x in range(0, width):
        layer = []
        for y in range(0, height):
            layer.append(tile_type)
        room.append(layer)
    return room