from models.type import Type

# Checks for overlapping objects => True == overlap
def collision_check(game,x,y,width,height):
    room = game.get_room_drawable()
    for xx in range(0, width):
        for yy in range(0, height):
            if x+xx >= game.room.width or y+yy >= game.room.height or x < 0 or y < 0: return True # Must stay in-bounds
            if room[x+xx][y+yy] == Type.FURNITURE: return True # Cannot collide with furniture
    return False

# Calculate if robot can maneuvre if the piece of furniture would be added
def furniture_collision_check(game,x,y,width,height):
    room = game.get_room_drawable()
    # Place furniture in game copy
    for xx in range(0, width):
        for yy in range(0, height):
            room[x+xx][y+yy] = Type.FURNITURE

    if (furniture_collision_check_internal(room)): return True # Check horizontally
    if (furniture_collision_check_internal(zip(*reversed(room)))): return True # Check vertically by rotating the room
    return False # Furniture fits horizontally and vertically

# Allow re-using of this code so that room can be checked horizontally and vertically
def furniture_collision_check_internal(room):
    # Per layer, check if there is at least a three block wide gap for the robot
    for l_pos,layer in enumerate(room):
        previous_block_was_furniture = True
        gap_length = 0
        for b_pos,block in enumerate(layer):
            # If the block is not furniture, there's a gap for the robot
            if block is not Type.FURNITURE:
                previous_block_was_furniture = False
                gap_length += 1
            # If the block is furniture, make sure there's a gap for the robot that's wide enough
            else:
                if gap_length < 3 and not previous_block_was_furniture: return True
                gap_length = 0
                previous_block_was_furniture = True
        if gap_length < 3: return True # Don't tolerate gaps at the end of the layer
    return False