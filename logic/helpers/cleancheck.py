from models.type import Type

def clean_check(room):
    dirty_tile_count = 0
    clean_tile_count = 0
    for layer in room:
        for tile in layer:
            if tile == Type.DIRTY or tile == Type.UNKNOWN:
                dirty_tile_count += 1
            else:
                clean_tile_count += 1
    return int(clean_tile_count / (dirty_tile_count + clean_tile_count) * 100)