from models.type import Type

# No dirty tiles can remain
def won(room):
    for layer in room:
        for tile in layer:
            if tile == Type.DIRTY:
                return False
    return True