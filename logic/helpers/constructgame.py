from logic.helpers.addfurniture import add_furniture
from models.game import Game
from models.robot import Robot
from models.dock import Dock
from models.furniture import Furniture

def construct_game(width,height,difficulty):
    game = Game(width,height,difficulty)
    add_furniture(game)
    game.robot = Robot(game)
    game.dock = Dock(game)
    return game