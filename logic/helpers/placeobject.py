import random

from logic.helpers.collisioncheck import collision_check, furniture_collision_check
from models.type import Type

def place_object(game,width,height,is_furniture=False):
    attempts = 0
    while 1:
        attempts += 1
        if attempts == 1000:
            print("Couldn't generate map properly! Lowering the difficulty will help")
            return None
        x = random.randint(0,game.room.width)
        y = random.randint(0,game.room.height)
        # Overlap with furniture not allowed
        if collision_check(game,x,y,width,height): continue
        # If furniture is being placed, make sure there is a 3x3 gap for the robot
        if is_furniture and furniture_collision_check(game,x,y,width,height): continue
        return (x,y)
