import random
from models.furniture import Furniture

def add_furniture(game):
    for i in range(0,game.difficulty):
        width = random.randint(2,6)
        height = random.randint(2,6)
        if (bool(random.getrandbits(1))): width,height = height,width # Random rotation
        new_furniture = Furniture(game,width,height)
        if new_furniture.success:
            game.furniture.append(new_furniture)
