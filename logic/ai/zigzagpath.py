from logic.ai.astar import astar
from logic.helpers.collisioncheck import collision_check

# A* from the top left of the map in a zigzag pattern
def zigzag_path(game_copy,path):
    if path is None: return path
    even_turn = True
    go_from_left_to_right = True
    # Go through every thrid layer so that the 3x3 robot can vacuum
    for y in range(0, game_copy.room.height, 3):
        if (y > game_copy.room.height - 3): y = game_copy.room.height - 3 # Stay in-bounds
        for i in range(0,2):
            x = 0
            if go_from_left_to_right: x = game_copy.room.width - 3

            # Calculate if furniture isn't in the way of the zigzag
            final_position = compensate_for_furniture(game_copy,y,x,go_from_left_to_right)
            if final_position is None: return path # Furniture in the way, return the remaining possible path
            else:
                y = final_position[0]
                x = final_position[1]

            path_piece = astar(game_copy, game_copy.robot.coordinates(), (y,x))
            if path_piece is None:
                return None # Stop if impossible
            print("Added A* path to zigzag to " + str((y,x)))
            game_copy.robot.follow_path(path_piece)
            path += path_piece
            # Determine if to A* to the left or right of the layer for zigzagging
            if even_turn:
                go_from_left_to_right = not go_from_left_to_right
                even_turn = False
            else: even_turn = True
    return path

# Mess with the x (and y) coordinates if furniture is in the way
def compensate_for_furniture(game_copy,y,x,go_from_left_to_right):
    while 1:
        if not collision_check(game_copy,y,x,3,3): return ((y,x))
        # Collision with furniture:
        # Move robot's end position further away from the wall
        # Go to next layer if it's impossible to sweep on the layer
        if go_from_left_to_right:
            x -= 1
            if x < 0:
                x = game_copy.room.width - 3
                y += 1
        else:
            x += 1
            if x > game_copy.room.width - 3:
                x = 0
                y += 1
        # Impossible to move further
        if y > game_copy.room.height - 3:
            return None