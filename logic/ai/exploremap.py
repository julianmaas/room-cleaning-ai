from logic.helpers.constructfromsensed import construct_from_sensed
from logic.helpers.wallcheck import *
from logic.helpers.cleancheck import clean_check
from models.directions import *

# Try to visualize the room by trying to find the four walls of the room
# Will return the map once it's sure it has found all four walls
# The map itself will still have many empty spots the robot must explore
def explore_map(game_copy,path):
    if path is None: return path
    relative_position = (0,0)
    absolute_position_start = game_copy.robot.coordinates()

    sensed_parts = {} # Per relative position, store what the robot sensed
    explored_directions = {} # Per relative position where the robot couldn't move, store the directions it tried
    current_direction = Directions.LEFT

    left_wall_found, right_wall_found, up_wall_found, down_wall_found = False, False, False, False
    while not left_wall_found or not right_wall_found or not up_wall_found or not down_wall_found:
        # Store the sensed tiles associated with the current location
        sensed = game_copy.robot.sense()
        sensed_parts.update({relative_position: sensed})

        # Check if a wall was found
        if not left_wall_found: left_wall_found = left_wall_check(sensed)
        if not right_wall_found: right_wall_found = right_wall_check(sensed)
        if not up_wall_found: up_wall_found = up_wall_check(sensed)
        if not down_wall_found: down_wall_found = down_wall_check(sensed)

        # Try to move in current direction, else go next direction
        if not game_copy.robot.move(current_direction):
            # If this tile was already explored, find a turn direction that wasn't yet explored with this tile
            if relative_position in explored_directions:
                turn_attempts = 0
                while turn_attempts < 3: # Make robot return if all options are exhausted
                    current_direction = next_direction(current_direction)
                    tile_directions = explored_directions.get(relative_position)
                    # If this direction wasn't explored yet, mark the direction as explored; direction is then used in next move
                    if current_direction not in tile_directions:
                        explored_directions.update({relative_position: tile_directions + [current_direction]})
                        break
                    turn_attempts += 1
            else:
                current_direction = next_direction(current_direction) # Tile not yet explored, go to next direction
                explored_directions.update({relative_position: [current_direction]}) # Store that a direction was taken at the tile
            print("Robot turned at: " + str(relative_position))
        else: # Could move
            path.append(game_copy.robot.coordinates())
            relative_position = update_relative_position(relative_position, current_direction)

    # Convert sensed parts to a single representation of the room
    print("Exploring the room cleant " + str(clean_check(game_copy.get_room_drawable())) + "% of the room in " + str(len(path)) + " tiles")
    game_copy.room.data = construct_from_sensed(sensed_parts, absolute_position_start, game_copy.robot.coordinates())
    print("The robot thinks it cleant " + str(clean_check(game_copy.room.data)) + "% of the room")

    return path

# Cycle through directions in a non-random(!) manner
def next_direction(direction):
    if (direction == Directions.LEFT): return Directions.DOWN
    if (direction == Directions.DOWN): return Directions.RIGHT
    if (direction == Directions.RIGHT): return Directions.UP
    if (direction == Directions.UP): return Directions.LEFT

# Relative position gets updated based on the direction the robot took
def update_relative_position(relative_position, direction):
    position = direction_to_position(direction)
    return (relative_position[0] + position[0], relative_position[1] + position[1])