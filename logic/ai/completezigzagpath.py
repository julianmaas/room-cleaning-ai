from logic.ai.exploretiles import explore_tiles
from logic.ai.zigzagpath import zigzag_path
from logic.helpers.cleancheck import clean_check
from models.type import Type

# Zigzag first to explore the room, then A* to empty tiles in the most ideal fashion
def complete_zigzag_path(game_copy,path):
    if path is None: return path
    path = zigzag_path(game_copy,path)
    if path is not None: print("Zigzag cleant up " + str(clean_check(game_copy.get_room_drawable())) + "% of the room in " + str(len(path)) + " tiles")
    path = explore_tiles(game_copy,path,Type.DIRTY)
    if path is not None: print("Robot has cleaned the whole room!")
    return path