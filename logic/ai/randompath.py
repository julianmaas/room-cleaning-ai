import random

from logic.helpers.cleancheck import clean_check
from logic.helpers.won import won
from models.directions import position_to_direction

# Expand a random path until the whole room is cleaned
def random_path(game_copy,path,print_intermediate=True):
    if path is None: return path
    iterations = 0
    while not won(game_copy.get_room_drawable()):
        # Stop if path is too unlikely
        if iterations == 100000:
            print("Random path is too unlikely to succeed, path cancelled")
            return None
        iterations += 1

        xmov,ymov = 0,0
        if random.randint(0,1):
            if random.randint(0,1):
                xmov = 1
            else:
                xmov = -1
        else:
            if random.randint(0,1):
                ymov = 1
            else:
                ymov = -1
        # Only store movement if the robot can move
        if game_copy.robot.move(position_to_direction(xmov,ymov)):
            path.append((game_copy.robot.x-xmov, game_copy.robot.y-ymov))

        if print_intermediate and iterations % 1000 is 0:
            print(str(clean_check(game_copy.get_room_drawable())) + "% of the room cleaned after " + str(iterations) + " iterations, current path length: " + str(len(path)))

    path.append(game_copy.robot.coordinates()) # Make sure robot cleans last tile
    return path