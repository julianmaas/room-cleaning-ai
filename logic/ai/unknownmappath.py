from logic.ai.exploremap import explore_map
from logic.ai.exploretiles import explore_tiles
from models.type import Type

# Try to visualize the room by first trying to find the four walls of the room
# Then A* the empty spots of the room and then A* the remaining dirty tiles
def unknown_map_path(game_copy,path):
    if path is None: return path
    path = explore_map(game_copy,path)
    if path is not None: print("Robot has explored the map boundaries!")
    path = explore_tiles(game_copy,path,Type.UNKNOWN)
    if path is not None: print("Robot has sensed the whole room!")
    path = explore_tiles(game_copy,path,Type.DIRTY)
    if path is not None: print("Robot has cleaned the whole room!")
    return path