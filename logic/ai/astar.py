import heapq

from logic.helpers.collisioncheck import collision_check
from models.node import Node

# Calculate path from one point of the room to the other point
def astar(game,start,end,uncertain=False):
    opened = []
    closed = []

    starting_node = Node(None,start,end)
    # Make sure it's possible for the robot to go to the ending node
    ending_node = None
    if uncertain: ending_node = get_ending_node(game,end)
    else: ending_node = Node(None,end,end)
    if ending_node is not None:
        heapq.heapify(opened)
        heapq.heappush(opened, starting_node)

    while len(opened) > 0:
        node = heapq.heappop(opened)
        closed.append(node)

        if node == ending_node:
            path = []
            current = node
            while current is not None:
                path.append(current.position)
                current = current.parent
            return path[::-1] # Return reversed path

        children = []
        for adjacent in [(0, -1), (0, 1), (-1, 0), (1, 0)]:
            position = (node.position[0] + adjacent[0], node.position[1] + adjacent[1])
            if collision_check(game, position[0], position[1], 3, 3):
                continue
            children.append(Node(node, position, end))

        for child in children:
            should_push = True
            for closed_node in closed:
                if child == closed_node:
                    should_push = False
            for opened_node in opened:
                if child == opened_node and child.cost > opened_node.cost:
                    should_push = False
            if should_push:
                heapq.heappush(opened, child)

    print("No path possible!")

# Push the end destination around if not certain if robot can reach the destination
def get_ending_node(game,end):
    for yy in range(0, 3):
        for xx in range(0, 3):
            if not collision_check(game, end[0]-xx, end[1]-yy, 3, 3):
                certain_end = (end[0]-xx, end[1]-yy)
                return Node(None,certain_end,certain_end)