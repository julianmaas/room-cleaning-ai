from logic.ai.exploremap import explore_map
from logic.ai.exploretiles import explore_tiles
from logic.ai.zigzagpath import zigzag_path
from models.type import Type

# Try to visualize the room by first trying to find the four walls of the room
# Then zigzag through the room to sense as many tiles as possible
# Then A* the empty spots of the room and then A* the remaining dirty tiles
def unknown_map_zigzag_path(game_copy,path):
    if path is None: return path
    path = explore_map(game_copy,path)
    if path is not None: print("Robot has explored the map boundaries!")
    path = zigzag_path(game_copy,path)
    if path is not None: print("Robot has zigzagged through the map!")
    path = explore_tiles(game_copy,path,Type.UNKNOWN)
    if path is not None: print("Robot has sensed the whole room!")
    path = explore_tiles(game_copy,path,Type.DIRTY)
    if path is not None: print("Robot has cleaned the whole room!")
    return path