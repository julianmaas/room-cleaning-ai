from logic.ai.astar import astar
from logic.helpers.findtile import find_tile
from logic.helpers.cleancheck import clean_check
from models.directions import *
from models.type import Type

# Explore all tiles of a given type
def explore_tiles(game_copy, path, tile_type):
    if path is None: return path
    tile_to_explore = find_tile(game_copy,tile_type)
    while tile_to_explore is not None:
        path_piece = astar(game_copy, game_copy.robot.coordinates(), tile_to_explore, uncertain=True)
        if path_piece is None: return None
        game_copy.robot.follow_path(path_piece)
        print("Room is now " + str(clean_check(game_copy.get_room_drawable())) + "% cleaned, according to the robot")
        path += path_piece
        tile_to_explore = find_tile(game_copy,tile_type)

    return path