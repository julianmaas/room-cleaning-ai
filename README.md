# Room Cleaning AI
We have just found out the vacuum robot we just bought came with a “*AI not included” tag in fine print.
Guess we'll have have to make our own.

1. Familiarize yourself with the controls using controls.png
2. Install all pip packages using requirements.txt
3. Run main.py