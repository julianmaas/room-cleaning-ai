import copy, time

from logic.ai.astar import astar
from logic.ai.exploremap import explore_map
from logic.ai.exploretiles import explore_tiles
from logic.ai.completezigzagpath import complete_zigzag_path
from logic.ai.randompath import random_path
from logic.ai.unknownmappath import unknown_map_path
from logic.ai.unknownmapzigzagpath import unknown_map_zigzag_path
from logic.ai.zigzagpath import zigzag_path

from logic.helpers.cleancheck import clean_check
from logic.helpers.collisioncheck import collision_check
from logic.helpers.placeobject import place_object

from models.directions import Directions, position_to_direction, direction_to_position
from models.type import Type

class Robot:
    def __init__(self,game):
        self.width = 3
        self.height = 3
        self.x,self.y = place_object(game,self.width,self.height)
        self.history = []
        self.game = game

    def coordinates(self):
        return(self.x,self.y)

    def print_statistics(self):
        print("Amount of tiles traversed: " + str(len(self.history)))
        print("Percentage of tiles cleaned: " + str(clean_check(self.game.get_room_drawable())))

    def print_history(self):
        print(self.history)

    def sense(self,print_environment=False):
        if print_environment:
            print("\nCurrently at location: " + str(self.coordinates()))
        room = self.game.get_room_drawable()
        room_view = []
        for x in range(0,5):
            layer = []
            for y in range(0,5):
                # Start scanning from the upper left of the robot
                xread = self.x - 1 + x
                yread = self.y - 1 + y
                # Out of bounds if sensing would scan something outside the room
                if xread < 0 or yread < 0 or xread >= self.game.room.width or yread >= self.game.room.height:
                    layer.append(Type.WALL)
                else: layer.append(room[xread][yread])
            room_view.append(layer)
            if print_environment: print(layer)
        if print_environment: print("")
        return room_view

    def move(self,direction):
        xmov,ymov = direction_to_position(direction)
        # Check if moving the robot would not cause overlap
        if not collision_check(self.game, self.x + xmov, self.y + ymov, 3, 3):
            # Move robot
            self.x+=xmov
            self.y+=ymov
            self.history.append(self.coordinates())
            # Tiles get cleaned
            for xx in range(0, 3):
                for yy in range(0, 3):
                    self.game.room.data[self.x+xx][self.y+yy] = Type.CLEAN
            return True
        return False

    def follow_path(self,path,print_length=False):
        if path is not None:
            if print_length: print("Path length: " + str(len(path)))
            for xmov,ymov in path:
                self.move(position_to_direction(xmov-self.x,ymov-self.y))
                # Show the robot moving as it tracks back
                self.game.draw()

    def dock(self):
        print("Calculating A*-path to the dock")
        start_time = time.time_ns() // 1_000_000
        path = astar(self.game, self.coordinates(), self.game.dock.coordinates())
        end_time = time.time_ns() // 1_000_000
        print("Calculated in " + str(end_time-start_time) + "ms")
        self.follow_path(path,print_length=True)

    def random(self):
        print("Calculating random path to clean all tiles")
        start_time = time.time_ns() // 1_000_000
        path = random_path(copy.deepcopy(self.game), [])
        end_time = time.time_ns() // 1_000_000
        print("Calculated in " + str(end_time-start_time) + "ms")
        self.follow_path(path,print_length=True)

    def average_random(self):
        print("Calculating average of 100 random paths to clean all tiles")
        start_time = time.time_ns() // 1_000_000
        sum = 0
        for i in range(0,100):
            result = len(random_path(copy.deepcopy(self.game), [], print_intermediate=False))
            if result is not None:
                print("Random path " + str(i+1) + "# has a length of " + str(result) + " tiles",)
                sum += result
            else: print("Random path " + str(i+1) + " FAILED!")
        end_time = time.time_ns() // 1_000_000
        print("\n")
        print("Average path length: " + str(sum//100))
        print("Calculated on average in " + str((end_time-start_time)//100) + "ms")

    def zigzag(self):
        print("Calculating path to clean tiles with a zigzag motion")
        start_time = time.time_ns() // 1_000_000
        path = zigzag_path(copy.deepcopy(self.game), [])
        end_time = time.time_ns() // 1_000_000
        print("Calculated in " + str(end_time-start_time) + "ms")
        self.follow_path(path,print_length=True)

    def search_and_clean(self):
        print("Calculating path to clean all tiles using heuristics")
        start_time = time.time_ns() // 1_000_000
        path = explore_tiles(copy.deepcopy(self.game), [], Type.DIRTY)
        end_time = time.time_ns() // 1_000_000
        print("Calculated in " + str(end_time-start_time) + "ms")
        self.follow_path(path,print_length=True)

    def complete_zigzag(self):
        print("Calculating path to clean tiles with a zigzag motion followed by targeting dirty tiles")
        start_time = time.time_ns() // 1_000_000
        path = complete_zigzag_path(copy.deepcopy(self.game), [])
        end_time = time.time_ns() // 1_000_000
        print("Calculated in " + str(end_time-start_time) + "ms")
        self.follow_path(path,print_length=True)

    def explore_map_boundaries(self):
        print("Calculating path to explore the boundaries of the map")
        start_time = time.time_ns() // 1_000_000
        path = explore_map(copy.deepcopy(self.game), [])
        end_time = time.time_ns() // 1_000_000
        print("Calculated in " + str(end_time-start_time) + "ms")
        self.follow_path(path,print_length=True)

    def clean_and_explore_map(self):
        print("Calculating path to clean all tiles without telling the robot the map layout")
        start_time = time.time_ns() // 1_000_000
        path = unknown_map_path(copy.deepcopy(self.game), [])
        end_time = time.time_ns() // 1_000_000
        print("Calculated in " + str(end_time-start_time) + "ms")
        self.follow_path(path,print_length=True)

    def clean_and_explore_map_zigzag(self):
        print("Calculating path to clean all tiles without telling the robot the map layout and by performing an additional zigzag scan")
        start_time = time.time_ns() // 1_000_000
        path = unknown_map_zigzag_path(copy.deepcopy(self.game), [])
        end_time = time.time_ns() // 1_000_000
        print("Calculated in " + str(end_time-start_time) + "ms")
        self.follow_path(path,print_length=True)
