from enum import Enum
class Directions(Enum):
    UP = 0
    DOWN = 1
    LEFT = 2
    RIGHT = 3

def position_to_direction(x,y):
    if x == -1 and y == 0: return Directions.UP
    elif x == 1 and y == 0: return Directions.DOWN
    elif x == 0 and y == -1: return Directions.LEFT
    elif x == 0 and y == 1: return Directions.RIGHT

def direction_to_position(direction):
    y,x = 0,0
    if (direction == Directions.UP): y,x = -1,0
    elif (direction == Directions.DOWN): y,x = 1,0
    elif (direction == Directions.LEFT): y,x = 0,-1
    elif (direction == Directions.RIGHT): y,x = 0,1
    return (y,x)