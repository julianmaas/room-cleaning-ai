from logic.helpers.addfurniture import add_furniture
from models.room import Room
from models.robot import Robot
from models.dock import Dock
from models.furniture import Furniture
from models.type import Type

class Game:
    def __init__(self,width,height,difficulty):
        self.difficulty = difficulty
        self.room = Room(width,height)
        self.furniture = []
        self.robot = None
        self.dock = None

    def get_room_drawable(self):
        room = self.room.get_data_copy()
        # Draw furniture
        for furniture in self.furniture:
            for x in range(furniture.width):
                for y in range(furniture.height):
                    room[furniture.x+x][furniture.y+y] = Type.FURNITURE
        # Draw dock
        if not self.dock == None:
            for x in range(self.dock.width):
                for y in range(self.dock.height):
                    room[self.dock.x+x][self.dock.y+y] = Type.DOCK
        # Draw robot
        if not self.robot == None:
            for x in range(self.robot.width):
                for y in range(self.robot.height):
                    room[self.robot.x+x][self.robot.y+y] = Type.ROBOT
        return room
