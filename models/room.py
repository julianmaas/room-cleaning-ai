from logic.helpers.generateroom import generate_room
from models.type import Type

class Room:
    def __init__(self,width,height):
        self.data = generate_room(width,height,Type.DIRTY)
        self.width = width
        self.height = height

    def get_data_copy(self):
        return list(map(list, self.data))

    def make_dirty(self):
        self.data = generate_room(self.width,self.height,Type.DIRTY)
