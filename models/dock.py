class Dock:
    def __init__(self,game):
        self.x = game.robot.x
        self.y = game.robot.y
        self.width = game.robot.width
        self.height = game.robot.height

    def coordinates(self):
        return(self.x,self.y)