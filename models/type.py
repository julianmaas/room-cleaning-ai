from enum import Enum
class Type(Enum):
    UNKNOWN = -1
    DIRTY = 0
    CLEAN = 1
    DOCK = 2
    ROBOT = 3
    FURNITURE = 4
    WALL = 5