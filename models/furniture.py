from logic.helpers.placeobject import place_object

class Furniture:
    def __init__(self,game,width,height):
        self.width = width
        self.height = height
        result = place_object(game,self.width,self.height,is_furniture=True)
        if result == None:
            self.success = False
        else:
            self.x,self.y = result
            self.success = True

    def coordinates(self):
        return(self.x,self.y)