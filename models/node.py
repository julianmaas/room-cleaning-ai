class Node:
    def __init__(self, parent=None, position=None, destination=None):
        self.parent = parent
        self.position = position
        if destination is not None:
            self.destination = destination
        else:
            self.destination = position
        self.cost = self.distance() + self.heuristic() # F (total node cost) = G (distance to start node) + H (heuristic)

    def distance(self):
        if self.parent is None:
            return 0
        return self.parent.distance() + 1

    def heuristic(self):
        # Heuristic consists of calculating the distance between this node and the destination
        a = abs(self.position[0]-self.destination[0])
        b = abs(self.position[1]-self.destination[1])
        return int(round( (a*a) + (b*b) ))

    def __eq__(self, other):
        return self.position == other.position

    def __lt__(self, other):
      return self.cost < other.cost

    def __gt__(self, other):
      return self.cost > other.cost